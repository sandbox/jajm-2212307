<?php

/**
 * @file
 * Database related functions.
 */

/**
 * Check if a taxonomy tree view page exists.
 */
function taxonomytreeview_page_exists($name) {
  $result = db_select('taxonomytreeview_page', 'p')
    ->fields('p')
    ->condition('name', $name)
    ->execute();

  return ($result->rowCount() > 0);
}

/**
 * Add a page in database.
 */
function taxonomytreeview_page_add($page) {
  $query = db_insert('taxonomytreeview_page')
    ->fields(array(
      'name' => $page['name'],
      'title' => $page['title'],
      'path' => $page['path'],
      'taxonomy_vocabulary_machine_name' => $page['taxonomy_vocabulary_machine_name'],
      'views_view_name' => $page['views_view_name'],
      'views_display_id' => $page['views_display_id'],
    ));

  $query->execute();
}

/**
 * Edit a page in database.
 */
function taxonomytreeview_page_edit($page) {
  $query = db_update('taxonomytreeview_page')
    ->fields(array(
      'title' => $page['title'],
      'path' => $page['path'],
      'taxonomy_vocabulary_machine_name' => $page['taxonomy_vocabulary_machine_name'],
      'views_view_name' => $page['views_view_name'],
      'views_display_id' => $page['views_display_id'],
    ))
    ->condition('name', $page['name']);

  $query->execute();
}

/**
 * Delete a page in database.
 */
function taxonomytreeview_page_delete($name) {
  db_delete('taxonomytreeview_page')
    ->condition('name', $name)
    ->execute();
}

/**
 * Return all taxonomy tree view pages.
 */
function taxonomytreeview_page_get_all() {
  $result = db_select('taxonomytreeview_page', 'p')
    ->fields('p')
    ->execute();

  return $result->fetchAll();
}

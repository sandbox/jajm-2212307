(function($) {

  window.addEventListener('load', function() {
    taxonomytreeview_collapse_all();
    $('.taxonomytreeview-term').click(function() {
      taxonomytreeview_term_toggle($(this));
    });

    if (location.hash.length > 0) {
        var tid = location.hash.substr(1);
        var term = $('.taxonomytreeview-term[data-tid="' + tid + '"]');
        if (term.length > 0) {
            taxonomytreeview_term_expand(term);
            var scrollY = Math.max(0, term.offset().top - 100);
            window.scroll(0, scrollY);
        }
    }
  });

  function taxonomytreeview_term_collapse($term) {
    var $contents = $term.siblings('.taxonomytreeview-contents');
    $contents.hide();
    $term.parent('li').removeClass('expanded');
    $term.parent('li').addClass('collapsed');
  }

  function taxonomytreeview_term_expand($term) {
    var tid = $term.attr('data-tid');
    taxonomytreeview_load_view(tid);
    var $contents = $term.siblings('.taxonomytreeview-contents');
    $contents.show();
    $term.parent('li').removeClass('collapsed');
    $term.parent('li').addClass('expanded');

    $term.parents('.collapsed').each(function() {
        taxonomytreeview_term_expand($(this).children('.taxonomytreeview-term'));
    });
  }

  function taxonomytreeview_term_toggle($term) {
    var $contents = $term.siblings('.taxonomytreeview-contents');
    if ($contents.is(':visible')) {
      taxonomytreeview_term_collapse($term);
    } else {
      taxonomytreeview_term_expand($term);
    }
  }

  function taxonomytreeview_collapse_all() {
    $('.taxonomytreeview-term').each(function() {
      taxonomytreeview_term_collapse($(this));
    });
  }

  function taxonomytreeview_load_view(tid) {
    var $container = $('#taxonomytreeview-view-container-' + tid);

    // Do not load views more than once.
    if (!$container.hasClass('data-view-loaded')) {

      // Add contextual filters (tid) to view arguments.
      var data = $.extend({}, Drupal.settings.taxonomytreeview, {
        'view_args': tid
      });

      // Views AJAX call.
      $.ajax({
        url: 'views/ajax',
        dataType: 'json',
        data: data,
        success: function(response) {
          $container
            .html(response[1].data)
            .addClass('data-view-loaded')
            .show();
        },
        error: function (xhr) {
          $container.html('Failed to load view (' + xhr.statusText + ' ' + xhr.status + ')');
        }
      });
      $container.html('Loading...');
    }
  }

})(jQuery);

<?php

/**
 * @file
 * Functions for rendering taxonomy tree view page.
 */

/**
 * Render a taxonomy tree as HTML (unordered list of items).
 */
function taxonomytreeview_page_tree_render($tree) {
  $output = '';

  if (is_array($tree) and !empty($tree)) {
    $output = '<ul>';

    foreach ($tree as $tid => $term) {
      $output .= '<li>';
      $output .= '<span class="taxonomytreeview-term" data-tid="' . $tid . '">';
      $output .= $term['name'];
      $output .= '</span>';
      $output .= '<div class="taxonomytreeview-contents">';
      $output .= taxonomytreeview_page_tree_render($term['children']);
      $output .= '<div id="taxonomytreeview-view-container-' . $tid . '" class="taxonomytreeview-view-container"></div>';
      $output .= '</div>';
      $output .= '</li>';
    }

    $output .= '</ul>';
  }

  return $output;
}

/**
 * Render HTML output for a taxonomy tree view page.
 */
function taxonomytreeview_page_render($page) {
  drupal_add_js(drupal_get_path('module', 'taxonomytreeview') . '/js/taxonomytreeview.page.js');
  drupal_add_css(drupal_get_path('module', 'taxonomytreeview') . '/css/taxonomytreeview.page.css');
  $settings = array(
    'taxonomytreeview' => array(
      'view_name' => $page->views_view_name,
      'view_display_id' => $page->views_display_id,
    ),
  );
  drupal_add_js($settings, 'setting');
  $output = '';
  $vocabulary = taxonomy_vocabulary_machine_name_load($page->taxonomy_vocabulary_machine_name);
  $terms = taxonomy_get_tree($vocabulary->vid);
  $tree = array();
  $terms_in_tree = array();
  foreach ($terms as $term) {
    if ($term->depth == 0) {
      $tree[$term->tid] = (array) $term;
      $tree[$term->tid]['children'] = array();
      $terms_in_tree[$term->tid] =& $tree[$term->tid];
    }
    else {
      $parent = $term->parents[0];
      $terms_in_tree[$parent]['children'][$term->tid] = (array) $term;
      $terms_in_tree[$parent]['children'][$term->tid]['children'] = array();
      $terms_in_tree[$term->tid] =& $terms_in_tree[$parent]['children'][$term->tid];
    }
  }

  $output = '<div class="taxonomytreeview-page">';
  $output .= taxonomytreeview_page_tree_render($tree);
  $output .= '</div>';

  return $output;
}

<?php

/**
 * @file
 * Admin pages
 */

/**
 * Display all existing pages.
 */
function taxonomytreeview_admin_pages() {
  module_load_include('inc', 'taxonomytreeview', 'includes/taxonomytreeview.db');

  $pages = taxonomytreeview_page_get_all();

  $header = array(
    'title' => t("Title"),
    'path' => t("Path"),
    'vocabulary' => t("Vocabulary"),
    'view' => t("View"),
    'operations' => array(
      'data' => t("Operations"),
      'colspan' => 2,
    ),
  );

  $rows = array();
  foreach ($pages as $page) {
    $row = array();

    $row['title'] = $page->title . ' (' . $page->name . ')';
    $row['path'] = $page->path;
    $row['vocabulary'] = $page->taxonomy_vocabulary_machine_name;
    $row['view'] = $page->views_view_name . ' (' . $page->views_display_id . ')';
    $base_path = 'admin/structure/taxonomytreeview/';
    $row['edit'] = l(t("edit"), $base_path . $page->name . '/edit');
    $row['delete'] = l(t("delete"), $base_path . $page->name . '/delete');

    $rows[] = $row;
  }

  $output = theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'empty' => t("There is no taxonomy tree view page defined yet."),
  ));

  return $output;
}

/**
 * Build the form for both add and edit pages.
 */
function taxonomytreeview_admin_page_form($form, &$form_state, $page = NULL) {
  form_load_include($form_state, 'inc', 'taxonomytreeview', 'includes/taxonomytreeview.db');

  $values = array();
  if (isset($form_state['values'])) {
    $values = $form_state['values'];
  }
  elseif ($page) {
    $values = (array) $page;
  }
  else {
    $values = array(
      'title' => NULL,
      'name' => NULL,
      'path' => NULL,
      'taxonomy_vocabulary_machine_name' => NULL,
      'views_view_name' => NULL,
      'views_display_id' => NULL,
    );
  }

  $form['#validate'] = array('taxonomytreeview_admin_page_form_validate');
  $form['#submit'] = array('taxonomytreeview_admin_page_form_submit');

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t("Title of the page."),
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => $values['title'],
  );
  $form['name'] = array(
    '#type' => 'machine_name',
    '#maxlength' => 255,
    '#default_value' => $values['name'],
    '#disabled' => isset($values['name']),
    '#machine_name' => array(
      'exists' => 'taxonomytreeview_page_exists',
      'source' => array('title'),
    ),
  );

  $path_description = "Path should only contains lowercase letters, digits, underscore(_), dash (-) or slash (/).";
  $path_description .= " It must start and end with a lowercase letter or a digit.";

  $form['path'] = array(
    '#type' => 'textfield',
    '#title' => t("Path"),
    '#description' => t($path_description),
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => $values['path'],
  );

  $vocabularies = taxonomy_vocabulary_get_names();
  $vocabularies_options = array();
  foreach ($vocabularies as $machine_name => $vocabulary) {
    $vocabularies_options[$machine_name] = $vocabulary->name;
  }
  $form['taxonomy_vocabulary_machine_name'] = array(
    '#type' => 'select',
    '#title' => t("Taxonomy vocabulary"),
    '#description' => t("Terms of this vocabulary will be displayed as a tree."),
    '#options' => $vocabularies_options,
    '#required' => TRUE,
    '#default_value' => $values['taxonomy_vocabulary_machine_name'],
  );

  $views = views_get_enabled_views();
  $views_options = array();
  foreach ($views as $name => $view) {
    if ($view->human_name) {
      $views_options[$name] = $view->human_name;
    }
    else {
      $views_options[$name] = $view->name;
    }
  }
  $form['views_view_name'] = array(
    '#type' => 'select',
    '#title' => t("View"),
    '#description' => t("View to render when user click on a taxonomy term."),
    '#options' => $views_options,
    '#required' => TRUE,
    '#default_value' => $values['views_view_name'],
    '#ajax' => array(
      'callback' => 'taxonomytreeview_admin_page_ajax_callback',
      'wrapper' => 'views_display_id',
      'effect' => 'fade',
    ),
  );

  $view_displays_options = array();
  if (isset($values['views_view_name'])) {
    $views_displays = $views[$values['views_view_name']]->display;
    foreach ($views_displays as $name => $display) {
      $view_displays_options[$name] = $display->display_title;
    }
  }
  $form['views_display_id'] = array(
    '#type' => 'select',
    '#title' => t("View display"),
    '#description' => t("View display to use for rendering the view. The display must have at least one contextual filter and the first contextual filter must be on taxonomy term id."),
    '#options' => $view_displays_options,
    '#required' => TRUE,
    '#default_value' => $values['views_display_id'],
    '#prefix' => '<div id="views_display_id">',
    '#suffix' => '</div>',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t("Save page"),
  );

  return $form;
}

/**
 * AJAX callback used when user select a view.
 */
function taxonomytreeview_admin_page_ajax_callback($form, $form_state) {
  return $form['views_display_id'];
}

/**
 * Validate callback for taxonomytreeview_admin_page_form.
 */
function taxonomytreeview_admin_page_form_validate($form, $form_state) {
  $values = $form_state['values'];

  if (!preg_match('/^[a-z0-9][a-z0-9_\/-]*[a-z0-9]$/', $values['path'])) {
    $message = "Path should only contains lowercase letters, digits, underscore(_), dash (-) or slash (/).";
    $message .= " It must start and end with a lowercase letter or a digit.";
    form_set_error('path', t($message));
  }
}

/**
 * Submit callback for taxonomytreeview_admin_page_form.
 */
function taxonomytreeview_admin_page_form_submit($form, $form_state) {
  $values = $form_state['values'];
  $page = array(
    'name' => $values['name'],
    'title' => $values['title'],
    'path' => $values['path'],
    'taxonomy_vocabulary_machine_name' => $values['taxonomy_vocabulary_machine_name'],
    'views_view_name' => $values['views_view_name'],
    'views_display_id' => $values['views_display_id'],
  );
  if (taxonomytreeview_page_exists($page['name'])) {
    taxonomytreeview_page_edit($page);
    drupal_set_message(t("Page '@title' successfully modified", array(
      '@title' => $page['title'],
    )));
  }
  else {
    taxonomytreeview_page_add($page);
    drupal_set_message(t("New page successfully created."));
  }

  menu_rebuild();

  drupal_goto('admin/structure/taxonomytreeview');
}

/**
 * Form to add a taxonomy tree view page.
 */
function taxonomytreeview_admin_page_add($form, &$form_state) {
  return taxonomytreeview_admin_page_form($form, $form_state);
}

/**
 * Form to edit a taxonomy tree view page.
 */
function taxonomytreeview_admin_page_edit($form, &$form_state, $page) {
  return taxonomytreeview_admin_page_form($form, $form_state, $page);
}

/**
 * Form to delete a taxonomy tree view page.
 */
function taxonomytreeview_admin_page_delete($form, &$form_state, $page) {
  $form['areyousure'] = array(
    '#markup' => t("Are you sure you want to delete page '@title' ?", array(
      '@title' => $page->title,
    )),
  );

  $form['name'] = array(
    '#type' => 'hidden',
    '#value' => $page->name,
  );

  $form['confirm'] = array(
    '#type' => 'submit',
    '#value' => t("Confirm deletion"),
    '#prefix' => '<div class="form-actions">',
    '#suffix' => '</div>',
  );

  return $form;
}

/**
 * Submit callback for taxonomytreeview_admin_page_delete.
 */
function taxonomytreeview_admin_page_delete_submit($form, &$form_state) {
  module_load_include('inc', 'taxonomytreeview', 'includes/taxonomytreeview.db');

  $name = $form_state['values']['name'];
  taxonomytreeview_page_delete($name);
  drupal_set_message(t("Taxonomy tree view page successfully deleted."));

  drupal_goto('admin/structure/taxonomytreeview');
}
